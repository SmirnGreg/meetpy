# meetpy
Repository for the Python Meeting @ MPIA.

## Meetings

| Topic | Chair | Speaker 1 | Speaker 2 | Status |
|------|-------|--------|---|---|
|which python| @smirngreg  |Zoltan for venv | @smirngreg for conda  |05.02.2020 |
|Unit tests |Felix | Greg | ... | 04.03.2020 |
|pip install myproject | Greg | Felix | ... | 18.03.2020
|Default configurations| Felix | TBD | TBD | TBA|
|Own website | Irina | TBD | TBD | TBA|
|Wolfram Mathematica | Gabriele |
|MatLab | Elad |
