# Tests
## Unittests

Python has an easy to use `pytest` framework to write and run tests, see [example.py] and tests 1-3 for the example.

It can be run as simple as 
```bash
pytest [optional filename, default: test_*.py *_test.py]
```

### Hypothesis

`hypothesis` is an extentions which allows creating a lot of test cases if you have a pair of inverse functions.
Sometemis it is very useful.

## Integration tests

Unit tests are great, but integration tests are usually easier to write, and cover more code at once. 
Simpliest integration test is shown in [integration.py] with some program which is *too difficult to write test*,
and [test_4_integration.bash] is a trivial run-and-diff with precalculated output.




