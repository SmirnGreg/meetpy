import pytest

import example

@pytest.mark.parametrize(
    ("who", "expected"),
    (
            (1, 'Hello 1'),
            ('world', 'Hello world'),
            (None, 'Hello world'),
            ([1, 2], 'Hello [1, 2]')
    )
)
def test_parametrize_hello(who, expected):
    assert example.helloworld(who) == expected