# Python installations

## System python 

Every *nix systems comes with a python installation. After you install new system, get a new laptop, or connect to a remote computer, you can learn what version of python you have. For example, on a freshly installed Open Suse Leap 15.1 under WSL:

```bash
python --version  # Python 2.7.14
python3 --version  # Python 3.6.5
```

We also can learn where the python binary interpreter locates:

```bash
which python # /usr/bin/python
which python3  # /usr/bin/python3
```

It means that the first place where this fresh system finds python is located in `\usr\bin`. This location is the place where other system packages would look for python. On debian-based systems, such as Ubuntu, it is managed by `apt`. On OpenSuse the same duty is carried by `zypper`. Unfortunately, as you probably know already, both tools need superuser rights (sudo). But it is what you almost never had, unless it is your private computer. It is pretty obvious as they are going to deal with things installed in `/usr`, which is outside your home '~' directory, thus being system-wide. *It might be that there is a keyword allowing local installations with apt or zypper, but I never used them*.

```
echo $PATH  # /home/smirngreg/bin:/usr/local/bin:/usr/bin:/bin
```

For Python, you might be satisfied with the system version. On the other hand, I recommend you to go to the latest anaconda version, because it is stable enough and has all the main packages optimized, while also has new shiny features of one of the most recent Python release (f-strings, mmm). 
```
Python 2.7.17 - the last python2 release. Everyone MUST change to python3 since 1.1.2020
Python 3.6 - 23 Dec 2016
Python 3.7 - 27 Jun 2018 -- the latest anaconda release is built for python3.7
Python 3.8 - 14 Oct 2019
```
Python2 should be avoided since 01.01.2020, all code should be updated to python3. It is easily doable with `2to3` python standard packages. New code should not be developed under python2.

## Python packages

Besides the Python itself, we use a large number of side packages: numpy, scipy, astropy, emcee, etc. When we work with a system Python, there are two ways two install them:
1. With a system package manager (apt, zypper)
2. With `pip`

The both ways require sudo privilegies. I don't know what way is better. I feel that system package manager is a bit better as it is keeps consistensy of a system, but I feel that both ways are not perfect.

If you don't have sudo privilegies, but urgently need to install something to the system python, the safe way would be usage of `pip install --user <package>`, that installs the package to your home directory.

## venv

Link to the original website: https://realpython.com/python-virtual-environments-a-primer/

[Slides pdf.](venv.pdf)

## conda

Conda is a package manager for python, similar to pip, and the often come together (conda can install packages with pip setup.py scripts). 

Conda comes with one of two distributions: 
1. miniconda with the minimum set of packages
2. anaconda with 600 MB of packages, including Intel Math Kernel Library (MKL) replacing numpy standard library, spyder IDE, jupyter server, and others.

Conda allows you to have multiple installations of python (conda env) at the same time. 

[Ana]conda does not require sudo privilegies to install it. BUT on cluster there might be better-compiled own python distributrions, check with their helpdesk.

Intel has a python distibution compiled with icc/ifort available as conda repository. By practice, ifort is 3 times faster than icc. I benchmaked Intel Python on WSL on a laptop for matplotlib task (80 plots), and standard anaconda was faster.

### Anaconda installation 
Check latest https://www.anaconda.com/distribution/. Instruction for linux:
```bash
wget https://repo.anaconda.com/archive/Anaconda3-2019.10-Linux-x86_64.sh  #
bash Anaconda3-2019.10-Linux-x86_64.sh # yes, yes, yes ...
# READ WHAT IT TELLS YOU!
conda init bash  # zsh csh tsh ...
sh  # open new shell
```
Turn conda on and off:
```
conda activate <envname>
conda deactivate 
```
Environments: https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html


